import { Component, OnInit } from '@angular/core';
import { Post } from '../post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {


  posts : Post[] = [
    {
      title:'Mon premier Post',
      content:'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',
      loveIts:  1,
      created_at : new Date()
    },
    {
      title:'Mon deuxieme Post',
      content:'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',
      loveIts:  2,
      created_at : new Date()
    },
    {
      title:'Encore un Post',
      content:'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',
      loveIts:  0,
      created_at : new Date()
    }

  ]

  constructor() { }

  ngOnInit() {
  }

}
