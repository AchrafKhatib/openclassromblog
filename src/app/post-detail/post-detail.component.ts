import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../post';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  @Input() post : Post[];


  constructor() { }

  ngOnInit() {
  }

  getColor(loveIts){
    if (loveIts==1) {
      return 'green';
    }
    else if (loveIts==2){
      return 'red';
    }
  }
  onLove(){
    this.post.loveIts = 1;
  }
  onHate(){
    this.post.loveIts = 2;
  }

}
